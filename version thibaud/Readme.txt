BRFv4

La page new intex.html est la page que nous avons cr�e, celle index.html est celle original fournit avec le framework

Avant toute chose il faut savoir que BRFv4 est compos� de 2 parties
BRFv4 qui g�re la d�tection du visage et son suivit, ainsi que d'autre �l�ment li� au visage.
ThreeJS une biblioth�que de rendu 3D. ThreeJS permet de creer, g�rer, integrer des �l�ments 3D ou des sc�ne 3D.

Plusieurs exemples d'utilisation sont fournit avec le SDK. Ils se trouve dans js/examples/...
Les exemples fonctionne soit avec BRFv4 seul soit avec BRFv4 et ThreeJS combin�

Tous les exemples sont control� et utiliser via le meme fichier. Il s'agit du fichier BRFv4Demo.js, dans le dossier js. Il s'agit du fichier par defaut fournit avec le SDK.
Pour notre module nous en avons recreer un, il s'agit du fichier newBRFv4Demo.js.
Dans ce fichier (au debut) sont d�clare les chemins d'acces des autres fichiers, dans le var brfv4Example

var brfv4Example = {

	appId: "com.tastenkunst.brfv4.js.examples", // Choose your own app id. 8 chars minimum.
/**********************Declaration des chemin d'acces**************************/
	loader: { queuePreloader: null },	// preloading/example loading
	imageData: {						// image data source handling
		webcam: { stream: null },		// either webcam ...
		picture: {}						// ... or pictures/images
	},
	dom: {},							// html dom stuff
	gui: {},							// QuickSettings elements
	drawing: {},						// drawing the results using createJS
	drawing3d: {						// all 3D engine functions
		t3d: {}//,						// ThreeJS stuff
		//f3d: {}						// Flare3D stuff (coming later)
	},
	stats: {}							// fps meter
};
Via ce var vous pouvez acc�der aux autre fichiers mais aussi a leurs propres fonctions.

Chaque exemple d�fini aussi son propre chemin, l'exemple : ThreeJS_example.js : 

var t3d = brfv4Example.drawing3d.t3d;

Cette ligne permet d'eviter de r�utilise le meme path.
Ainsi les fonctions seront not� : t3d.AutreNom au lieu de brfv4Example.drawing3d.t3d.AutreNom

Mais on pour les appeler depuis un autre fichier, comme le newIndex.html, il faut faire par exemple : 
brfv4Example.drawing3d.t3d.nomDeFonction 

Cepand cela ne fonctionnera que si vous appeler une fonction d'un exemple actuellement lance.


Pour lance un exemple il faut obligatoirement pass� par le newBRFv4Demo.js et la fonction brfv4.start
Dans cette fonction on lui d�termine quel va etre l'exemple a lance, et quel est le mode de lancement video ou photo (attention il faut obligatoirement lance une video au 1er lancement sinon cela genere des problemes pour la suite).
Apres chaque lancement de la fonction il faut "stoper" le flux afin de pouvoir en lancer un autre par la suite via la fonction brfv4Example.loader.loadExample([""], onExampleLoaded); quand on est en flux video et la fonction  brfv4Example.loader.loadExample([p], ); quand on est en mode picture (ou photo).


Notre module n'utilise que 2 exemples fournit avec le SDK, png_mask_overlay.js et ThreeJS_example.js (dans js/examples/face_tracking)
Nous avons cependant aussi cr�er notre propre exemple en nous bassant sur ThreeJS_example.js, l'exemple newThreeJS.js, il dispose des meme chemins d'acces que l'original mais permet d'afficher des objets 3D en se d�tachant du suivit de visage.

Les 2 exemple de ThreeJS fonctionne avec le fichier BRFv4Drawing3DUtils_ThreeJS.js dans js/utils
Ce fichier est responsable de l'affichage des elements 3D, de leur position, rotation, taille ...
En flux video la fonction  t3d.update est reactualise en permanence (suivit en temps r�el des visages).
Par cons�quent pour changer en direct un parametre de l'objet 3D (taille, position, rotation ...) il suffit de modifier une valuer global (cf code).



